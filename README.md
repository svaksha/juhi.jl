# juhi.jl

My sandbox...!

----

# LICENSE

+ COPYRIGHT © 2016-Now [SVAKSHA][SVAKSHA] All Rights Reserved. 
+ The __software source code__ for `juhi.jl` and all code snippets used in the documentation files are released under the [AGPLv3 License](http://www.gnu.org/licenses/agpl.html) License as detailed in the [LICENSE.md](https://github.com/svaksha/juhi.jl/blob/master/LICENSE.md) file. All copies and forks of this work must retain the Copyright and the Licence file for the source codes along with this permission notice in all copies or substantial portions of the Software.

[SVAKSHA]: http://svaksha.com/pages/Bio "SVAKSHA"
[Julia]: http://julialang.org "Julia"
[juhi]: https://github.com/svaksha/juhi.jl/ "juhi.jl"
